const axios = require('axios');
const url = "https://jsonplaceholder.typicode.com/users";

// axios.get(url).then(response => {
//     response.data.forEach(element => {
//         console.log(element.username);
//     });
// });

axios.post(url, {
    username: "Foo Bar",
    email: "foo@bar.com"
})
axios.post(url).then(response => console.log(response.data))
