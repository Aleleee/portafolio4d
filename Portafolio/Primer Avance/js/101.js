// Muestra el mensaje "Hola mundo" en la consola
console.log("Hola mundo")

// Asigna la cadena "Foo bar" a la variable s
var s = "Foo bar"

// Asigna los valores 90 y 89 a las variables x e y respectivamente
let x = 90
var y = 89

// Muestra el valor de la variable s en la consola
console.log(s)

// Muestra la suma de x e y en la consola
console.log(x + y)

// Cambia el valor de la variable s a true
s = true

// Muestra el nuevo valor de la variable s en la consola
console.log(s)

// Crea un array con varios tipos de datos
array = [1,2,3,4,5, "Foo", "Bar", true, false, 2.34, 4.23]

// Muestra el elemento en la posición 5 del array en la consola
console.log(array[5])

// Crea un objeto con propiedades y valores
var obj = {
    first_name: "Foo",
    last_name: "Bar", 
    age: 23,  
    city: "TJ", 
    status: true
}

// Muestra el objeto completo en la consola
// console.log(obj)

// Muestra el valor de la propiedad "first_name" del objeto en la consola
// console.log(obj["first_name"])

// Muestra el valor de la propiedad "last_name" del objeto en la consola
// console.log(obj.last_name)

// Utiliza un bucle for para mostrar números del 0 al 95 de 5 en 5
 for (let i = 0; i < 100; i+=5) {
     console.log(i)
 }

// Utiliza un bucle for para mostrar cada elemento del array en la consola
for (let i = 0; i < array.length; i++) {    
    console.log(array[i])
}

// Utiliza un bucle for...of para mostrar cada elemento del array en la consola
 for (let i of array) {
     console.log(i)
}

// Utiliza un bucle for para mostrar cada propiedad y valor del objeto en la consola
for (let key of Object.keys(obj)) {
    console.log(key+ ":" + obj[key])
}

// Utiliza un bucle for...in para mostrar cada propiedad y valor del objeto en la consola
// for (let key in obj) {
//     console.log(key + ":" + obj[key])
// }

// Utiliza un bucle while para mostrar números del 0 al 95 de 5 en 5
// var i = 0
// while (i < 100) {
//     console.log(i)
//     i+=5
// }

// Utiliza un bucle do...while para mostrar números del 500 al 995 de 5 en 5
// var k = 500
// do {
//     console.log(k)
//     k += 5
// } while (k < 1000);

// Define una función que calcula el volumen de un prisma
// var prism = function(l, w, h) {
//     return l * w * h
// }

// Define una función que devuelve una función que calcula el volumen de un prisma
// function prisma(l) {
//     return function(w) {
//         return function(h) {
//             return l * w * h
//         }
//     }
    
// }

// Muestra el volumen de un prisma en la consola
console.log("Volumen del prisma: " + prisma(23) (12) (56))

// Asigna el valor "Foo Bar" a la variable msg
var msg = "Foo Bar"

// Define una función que muestra un mensaje y devuelve 45
 const foo = (function(msg) {
    console.log("I am the IIFE " + msg)
     return 45;
})(msg);

// Muestra el valor devuelto por la función en la consola
 console.log(foo)

// Define una función con nombre que suma dos números
var namedSum = function sum (a, b) {
     return a + b
}

// Muestra el resultado de la suma en la consola
console.log("El resultado es: " + namedSum(1,2))

// Define una función que muestra "Hello" cierto número de veces
var say = function say(times) {
    say = undefined

     if (times > 0) {
        console.log("Hello")
        say(times - 1)
    }
}

// Asigna la función a otra variable y cambia la referencia de la función original
// var saysay = say

// Asigna un nuevo valor a la variable originalmente referente a la función
// say = "Oops!"

// Llama a la función referenciada, que mostrará "Hello" 5 veces
// saysay(5)

// Define una función con un parámetro y la llama sin argumentos
function foo(msg="World") {
    console.log(msg)   
}
foo()
