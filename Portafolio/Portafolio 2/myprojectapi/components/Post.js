import React from 'react';
import { FlatList, View, Text } from "react-native-web";

const Post = ({ response }) => { 
  return (
    <View>
      <FlatList
        data={response}
        renderItem={({ item }) => (
          <View>
            <Text>Id Post: {item.id}</Text>
            <Text>Title Post: {item.title}</Text>
          </View>
        )}
        keyExtractor={item => item.id.toString()}
      />
    </View>
  );
};

export default Post;
