import react from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';

import HomeScreen from './screens/HomeScreen';
import Profile from './screens/Profile';
import SettingScreen from './screens/SettingScreen';
import StackScreen from './screens/StackScreen';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

function MyStack(){
    return(
        <Stack.Navigator>
            <Stack.Screen name='HomeScreen' component={HomeScreen} />
            <Stack.Screen name='Stack' component={StackScreen} />
        </Stack.Navigator>
    );    
}


function MyTab(){
    return(
        <Tab.Navigator>
            <Tab.Screen name='Home' component={MyStack} />
            <Tab.Screen name='Settings' component={SettingScreen} />
            <Tab.Screen name='Profile' component={Profile} />
        </Tab.Navigator>
    );
    
}

export default function Navigation(){
    return(
        <NavigationContainer>
            <MyTab />
        </NavigationContainer>
    );
}