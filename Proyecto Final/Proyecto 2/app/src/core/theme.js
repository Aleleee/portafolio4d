import { DefaultTheme } from 'react-native-paper'

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    text: '#000000',
    primary: '#133036',
    secondary: '#414757',
    error: '#f13a59',
  },
}

export const COLORS = {
  primary: '#133036',
  white: "#FFFFFF",
  gray: "#ECF0F4",
}

export const SIZES = {
  // Global SIZES
  base: 8,
  font: 14,
  radius: 30,
  padding: 8,
  padding2: 12,
  padding3: 16,

  // FONTS Sizes
  largeTitle: 50,
  h1: 30,
  h2: 22,
  h3: 20,
  h4: 18,
  body1: 30,
  body2: 20,
  body3: 16,
  body4: 14,

}

export const FONTS = {
  largeTitle: {
      fontFamily: 'Gilroy-ExtraBold',
      fontSize: SIZES.largeTitle,
      lineHeight: 55,
  },
  h1: { fontFamily: 'Gilroy-ExtraBold', fontSize: SIZES.h1, lineHeight: 36 },
  h2: { fontFamily: 'Gilroy-ExtraBold', fontSize: SIZES.h2, lineHeight: 30 },
  h3: { fontFamily: 'Gilroy-ExtraBold', fontSize: SIZES.h3, lineHeight: 22 },
  h4: { fontFamily: 'Gilroy-ExtraBold', fontSize: SIZES.h4, lineHeight: 20 },
  body1: { fontFamily: 'Gilroy-Light', fontSize: SIZES.body1, lineHeight: 36 },
  body2: { fontFamily: 'Gilroy-Light', fontSize: SIZES.body2, lineHeight: 30 },
  body3: { fontFamily: 'Gilroy-Light', fontSize: SIZES.body3, lineHeight: 22 },
  body4: { fontFamily: 'Gilroy-Light', fontSize: SIZES.body4, lineHeight: 20 },
}

const appTheme = { COLORS, SIZES, FONTS }

export default appTheme