export function emailValidator(email) {
  const re = /^\d{1}$/;
  if (!email) return "Este campo no puede estar vacío";
  if (!re.test(email)) return 'Por favor, ingresa un número de empleado válido de 10 dígitos';
  return '';
}
