import React, { useState } from 'react';
import { Alert } from 'react-native';
import { SERVER_IP } from '../config';
import axios from 'axios';

// Componentes personalizados y estilos
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import { theme } from '../core/theme';

// Helpers para validación
import { emailValidator } from '../helpers/emailValidator';
import { passwordValidator } from '../helpers/passwordValidator';

const ip = SERVER_IP;

export default function LoginScreen({ navigation }) {
  const [email, setEmail] = useState({ value: '', error: '' });
  const [password, setPassword] = useState({ value: '', error: '' });

  const handleLogin = async () => {
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);

    if (emailError || passwordError) {
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      return;
    }

    const url = `http://${ip}:8000/api/auth/login`;

    try {
      const response = await axios.post(url, {
        usuario_id: email.value,
        password: password.value,
      });

      Alert.alert('Éxito', 'Inicio de sesión exitoso');
      navigation.navigate('SolicitudesScreen');
    } catch (error) {
      console.error(error);
      Alert.alert('Error', 'Falló el inicio de sesión');
    }
  };

  return (
    <Background>
      <Logo />
      <TextInput
        label="Número de empleado"
        returnKeyType="next"
        value={email.value}
        onChangeText={(text) => setEmail({ value: text, error: '' })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />
      <TextInput
        label="Contraseña"
        returnKeyType="done"
        value={password.value}
        onChangeText={(text) => setPassword({ value: text, error: '' })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <Button mode="contained" onPress={handleLogin}>
        Iniciar sesión
      </Button>
    </Background>
  );
}
