import React from 'react';
import { StyleSheet, TextInput, View, TouchableOpacity } from 'react-native';
import Background from '../../components/Background';
import BackButton from '../../components/BackButton';
import Header from '../../components/Header';

const SolicitudesScreen = ({ onAccept, navigation }) => {
  const [taskName, setTaskName] = React.useState('');
  const [place, setPlace] = React.useState('');
  const [machineNumber, setMachineNumber] = React.useState('');
  const [date, setDate] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [priority, setPriority] = React.useState('');
  const [time, setTime] = React.useState('');
  const [supervisor, setSupervisor] = React.useState('');

  return (
    <Background>
    <Header>Solicitudes</Header>
    <View style={styles.container}>
      <TextInput
        placeholder="Nombre de la tarea"
        style={styles.input}
        value={taskName}
        onChangeText={setTaskName}
      />
      <TextInput
        placeholder="Lugar"
        style={styles.input}
        value={place}
        onChangeText={setPlace}
      />
      <TextInput
        placeholder="Número de la máquina"
        style={styles.input}
        value={machineNumber}
        onChangeText={setMachineNumber}
      />
      <TextInput
        placeholder="Fecha"
        style={styles.input}
        value={date}
        onChangeText={setDate}
      />
      <TextInput
        placeholder="Descripción del problema"
        style={styles.input}
        value={description}
        onChangeText={setDescription}
      />
      <TextInput
        placeholder="Prioridad"
        style={styles.input}
        value={priority}
        onChangeText={setPriority}
      />
      <TextInput
        placeholder="Hora"
        style={styles.input}
        value={time}
        onChangeText={setTime}
      />
      <TextInput
        placeholder="Nombre del supervisor"
        style={styles.input}
        value={supervisor}
        onChangeText={setSupervisor}
      />
      <TouchableOpacity
        onPress={() => onAccept({
          taskName,
          place,
          machineNumber,
          date,
          description,
          priority,
          time,
          supervisor,
        })}
        style={styles.acceptButton}
      >
      <TextInput style={styles.buttonText}>Aceptar</TextInput>
      </TouchableOpacity>
    </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  input: {
    height: 40,
    marginVertical: 8,
    borderWidth: 1,
    padding: 10,
    width: '100%',
    borderRadius: 5,
  },
  acceptButton: {
    backgroundColor: 'green',
    padding: 15,
    marginTop: 10,
    borderRadius: 5,
    width: '100%',
    alignItems: 'center',
  },
});

export default SolicitudesScreen;