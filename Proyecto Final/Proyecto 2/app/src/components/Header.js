import React from 'react';
import { StyleSheet, View, Text } from 'react-native'; // Usa Text de react-native directamente
import { theme } from '../core/theme';

export default function Header(props) {
  return (
    <View style={styles.headerContainer}>
      <Text style={styles.header} {...props} />
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    alignSelf: 'stretch',
    backgroundColor: '#133036',
    paddingVertical: 25,
  },
  header: {
    fontSize: 25,
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'Gilroy-ExtraBold', // Asegúrate de que este nombre coincida exactamente con como lo cargaste
    textAlign: 'center',
  },
});
