const mysql = require('mysql');
const config = require('../config');



const dbconfig = {
    host: config.mysql.host,
    user: config.mysql.user,
    password: config.mysql.password,
    database: config.mysql.database
};

let conexion;

function conmysql(){
    conexion = mysql.createConnection(dbconfig);
    return new Promise((resolve, reject) => {
        if (conexion) {
            console.log('Conectado a la base de datos');
            resolve();
        } else {
            console.log('No se pudo conectar a la base de datos');
            reject();

        }
    });
}

conmysql();

function all(table) { 
    return new Promise((resolve, reject) => {
        if (conexion) {
            const sql = `SELECT * FROM ${table} WHERE activo = 1`;
            const query = conexion.query(sql, (err, result) => {
                if (err) reject(err);
                resolve(result);
            });
        }        
    })
}

function one(table, id) { 
    return new Promise((resolve, reject) => {
        conexion.query(`SELECT * FROM ${table} WHERE id = ${id}`, (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}

function add(table, data) { 
    return new Promise((resolve, reject) => {
        conexion.query(`INSERT INTO ${table} SET ? ON DUPLICATE KEY UPDATE ?`, [data, data], (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}


function eraseone(table, data) { 
    return new Promise((resolve, reject) => {
        conexion.query(`DELETE FROM ${table} WHERE id = ?`, [data.id], (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}

function disable(table, data) { 
    return new Promise((resolve, reject) => {
        conexion.query(`UPDATE ${table} SET activo = 0 WHERE id = ?`, [data.id], (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}

function query(TABLE, consult) { 
    return new Promise((resolve, reject) => {
        conexion.query(`SELECT * FROM ${TABLE} WHERE ?`, consult, (err, result) => {
            return err ? reject(err) : resolve(result[0])
        })     
    })

}

function changePassword(TABLE, data) { 
    return new Promise((resolve, reject) => {
        conexion.query(`UPDATE ${TABLE} SET password = ? WHERE id = ?`, [data.password, data.id], (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}



module.exports = {
    all,
    one,
    add,
    disable,
    eraseone,
    query,
    changePassword
}