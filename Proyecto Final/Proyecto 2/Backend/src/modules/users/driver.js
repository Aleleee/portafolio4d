const response = require('../../connect/response');

const auth = require('../auth');

const TABLE = 'Usuario';


module.exports = function(dbInyector){

    let db = dbInyector;

    if(!db){
        db = require('../../DB/mysql');
    }

    function all(){
        return db.all(TABLE)
    }
    
    function one(id){
        return db.one(TABLE, id)
    }

    function validateKeys(body){
        let values = Object.values(body);
        let res = true;

        for (let index = 1; index < values.length; index++) {

            if( values[index] == "" ) {
                console.log("Entro al if", values[index]);
                res = false;
            }
        }
        console.log(res);
        return res;
    }
    
    async function add(body) {
        const user = {
            id: body.id,
            nombrePila: body.nombrePila,
            apPat: body.apPat,
            apMat: body.apMat,
            rol: body.rol,
            activo: body.activo
        };
        
        

        if(validateKeys(body)){
            
        try {

            let userResponse = await db.add(TABLE, user);

            let insertId = userResponse.insertId;
    
            if (insertId) {
                const authBody = {
                    usuario_id: insertId,
                    password: body.password
                };
                await auth.add(authBody);
                console.log("Usuario y credenciales de autenticación insertados correctamente.");
            }
        } catch (error) {
            console.error("Error al insertar el usuario o las credenciales de autenticación:", error);
            throw error;
        }
     } else {
         throw new Error('Todos los campos son obligatorios');
     }
    }

    
    
    function eraseone(body){
        return db.eraseone(TABLE, body)
    }

    function disable(body){
        return db.disable(TABLE, body)
    }
    
    return {
        all,
        one,
        add,
        eraseone,
        disable,
        validateKeys,
    }
}