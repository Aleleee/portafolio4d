import React from "react";
import { View, Text, FlatList } from "react-native";
import { StyleSheet } from "react-native";

const TodoList = ({ todos }) => {
  return (
    <FlatList
      data={todos}
      keyExtractor={({ id }) => id.toString()}
      renderItem={({ item }) => (
        <View style={styles.listItem}>
          <Text style={styles.listItemText}>
            ID: {item.id}
            {item.title && `: ${item.title}`}
          </Text>
          {item.userId && (
            <Text style={styles.userIdText}>UserID: {item.userId}</Text>
          )}
        </View>
      )}
    />
  );
};
const styles = StyleSheet.create({
  listItem: {
    padding: 14,
    marginVertical: 8,
    backgroundColor: "#1B3A68",
    borderColor: "#D50A0A",
    borderWidth: 1,
    borderRadius: 5,
  },
  listItemText: {
    fontSize: 18,
    color: "#FFFFFF",
  },
  userIdText: {
    fontSize: 16,
    color: "#FFFFFF",
  },
});

export default TodoList;
