import React, { useState, useEffect } from "react";
import {
  TouchableOpacity,
  View,
  ActivityIndicator,
  SafeAreaView,
  Platform,
  StyleSheet,
  Image,
  Text,
  ScrollView,
} from "react-native";
import { fetchTodos } from "./api";
import TodoList from "./TodoList";

const App = () => {
  const [isLoading, setLoading] = useState(true);
  const [todos, setTodos] = useState([]);
  const [filter, setFilter] = useState(null);

  useEffect(() => {
    fetchTodos()
      .then(setTodos)
      .catch(console.error)
      .finally(() => setLoading(false));
  }, []);

  const filteredTodos = todos
    .filter((todo) => {
      if (filter === "completed") return todo.completed;
      if (filter === "not-completed") return !todo.completed;
      return true;
    })
    .map((todo) => {
      switch (filter) {
        case "ids":
          return { id: todo.id };
        case "ids-titles":
          return { id: todo.id, title: todo.title };
        case "ids-userids":
          return { id: todo.id, userId: todo.userId };
        case "resolved-ids-userids":
          return todo.completed ? { id: todo.id, userId: todo.userId } : null;
        case "unresolved-ids-userids":
          return !todo.completed ? { id: todo.id, userId: todo.userId } : null;
        default:
          return todo;
      }
    })
    .filter(Boolean);

  if (isLoading) return <ActivityIndicator />;

  return (
    <SafeAreaView style={styles.safeArea}>
      <ScrollView contentContainerStyle={styles.scrollViewContent}>
        <View style={styles.container}>
          <View style={styles.header}>
            <Image
              source={require("./assets/nfl-logo.png")}
              style={styles.logo}
              resizeMode="contain"
            />
            <Text style={styles.title}>National Football League</Text>
          </View>
          <TouchableOpacity
            style={styles.button}
            onPress={() => setFilter("ids")}
          >
            <Text style={styles.buttonText}>Todos los pendientes</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => setFilter("ids-titles")}
          >
            <Text style={styles.buttonText}>
              Todos los pendientes (IDs y Títulos)
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => setFilter("not-completed")}
          >
            <Text style={styles.buttonText}>Pendientes sin resolver</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => setFilter("completed")}
          >
            <Text style={styles.buttonText}>Pendientes resueltos</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => setFilter("ids-userids")}
          >
            <Text style={styles.buttonText}>
              Todos los pendientes (IDs y UserIDs)
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => setFilter("resolved-ids-userids")}
          >
            <Text style={styles.buttonText}>
              Pendientes resueltos (IDs y UserIDs)
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => setFilter("unresolved-ids-userids")}
          >
            <Text style={styles.buttonText}>
              Pendientes sin resolver (IDs y UserIDs)
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => setFilter(null)}
          >
            <Text style={styles.buttonText}>Mostrar todos</Text>
          </TouchableOpacity>
          <TodoList todos={filteredTodos} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  button: {
    marginVertical: 10,
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderRadius: 25,
    backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center",
    ...Platform.select({
      ios: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 18,
    fontWeight: "600",
  },
  safeArea: {
    flex: 1,
    backgroundColor: "#0A2342",
  },
  container: {
    flexGrow: 1,
    padding: 20,
    backgroundColor: "white",
  },
  header: {
    alignItems: "center",
    marginBottom: 20,
  },
  logo: {
    width: 120,
    height: 60,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginTop: 10,
    color: "red",
  },
  scrollViewContent: {
    flexGrow: 1,
  },
});

export default App;
